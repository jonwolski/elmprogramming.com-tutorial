#!
set -euo pipefail

find src post-app -name *.elm  | xargs  elm make --output /dev/null
