module Signup exposing (User)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Browser


type alias User =
   { name : String
   , email : String
   , password : String
   , loggedIn : Bool
   }


initialModel : User
initialModel =
   { name = ""
   , email = ""
   , password = ""
   , loggedIn = False
   }


view : User -> Html Msg
view user =
    div []
        [ h1 [] [ text "Sign up" ]
        , Html.form []
            [ div []
                [ text "Name"
                , input [ id "name", type_ "text", onInput SaveName ] []
                ]
            , div []
                [ text "E-mail"
                , input [ id "email", type_ "text", onInput SaveEmail ] []
                ]
            , div []
                [ text "Password"
                , input [ id "password", type_ "password", onInput SavePassword ] []
                ]
            , div []
                [ button [ type_ "Submit", onClick Signup ]
                    [ text "create my account" ]
                ]
            ]
       ]

type Msg
    = SaveName String
    | SaveEmail String
    | SavePassword String
    | Signup

type alias Model = User

update : Msg -> Model -> Model
update msg user =
    case msg of
        SaveName name ->
            { user | name = name }

        SaveEmail email ->
            { user | email = email }

        SavePassword password ->
            { user | password = password }

        Signup ->
            { user | loggedIn = True }


main : Program () User Msg
main =
    Browser.sandbox
        { init = initialModel
        , view = view
        , update = update
        }


