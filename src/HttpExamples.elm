module HttpExamples exposing (Model)

import Browser
import Html exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Decode exposing (Decoder, Error(..), field, int, list, map3, string)
import RemoteData exposing (RemoteData, WebData)


type alias Post =
    { id : Int
    , title : String
    , author : String
    }


type alias Model =
    { posts : WebData (List Post)
    }


view : Model -> Html Msg
view model =
    div []
        [ div []
            [ button [ onClick SendHttpRequest ]
                [ text "Get data from server" ]
            , viewPostsOrError model
            ]
        ]


viewPostsOrError : Model -> Html Msg
viewPostsOrError model =
    case model.posts of
        RemoteData.NotAsked ->
            text ""

        RemoteData.Loading ->
            h3 [] [ text "loading …" ]

        RemoteData.Failure httpError ->
            viewError (buildErrorMessage httpError)

        RemoteData.Success posts ->
            viewPosts posts


viewError : String -> Html Msg
viewError errorMessage =
    let
        errorHeading =
            "Fetching data failed"
    in
    div []
        [ h3 [] [ text errorHeading ]
        , text ("Error: " ++ errorMessage)
        ]


viewPosts : List Post -> Html Msg
viewPosts posts =
    main_ []
        (List.map viewPost posts)


viewPost : Post -> Html Msg
viewPost post =
    article []
        [ h2 [] [ text post.title ]
        , div []
            [ cite [] [ text ("by " ++ post.author) ]
            ]
        , div []
            [ text (String.fromInt post.id) ]
        ]


viewNickname : String -> Html Msg
viewNickname nickname =
    li [] [ text nickname ]


postDecoder : Decoder Post
postDecoder =
    map3 Post
        (field "id" int)
        (field "title" string)
        (field "author" string)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SendHttpRequest ->
            ( { model | posts = RemoteData.Loading }, getPosts )

        DataReceived response ->
            ( { model | posts = response }, Cmd.none )


type Msg
    = SendHttpRequest
    | DataReceived (WebData (List Post))


postsUrl : String
postsUrl =
    "http://localhost:5019/posts"


getPosts : Cmd Msg
getPosts =
    Http.get
        { url = postsUrl
        , expect =
            list postDecoder
                |> Http.expectJson (RemoteData.fromResult >> DataReceived)
        }


textFromErrorMessage : Maybe String -> List (Html msg)
textFromErrorMessage maybe =
    case maybe of
        Nothing ->
            []

        Just something ->
            [ text something ]


initialModel : ( Model, Cmd Msg )
initialModel =
    ( { posts = RemoteData.NotAsked }, getPosts )


main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> initialModel
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }


buildErrorMessage : Http.Error -> String
buildErrorMessage httpError =
    case httpError of
        Http.BadUrl message ->
            message

        Http.Timeout ->
            "Server is taking too long to respond. Please try again later."

        Http.NetworkError ->
            "Unable to reach server."

        Http.BadStatus statusCode ->
            "Request failed with status code: " ++ String.fromInt statusCode

        Http.BadBody message ->
            message
